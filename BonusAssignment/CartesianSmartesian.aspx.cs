﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssignment
{
    public partial class CartesianSmartesian : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void Find_Quadrant(object sender, EventArgs e)
        {
            //Checking if client side validation has no error
            if (!Page.IsValid)
            {
                return;
            }

            //initiating variables for two coordinates
            double x = Convert.ToDouble(xCoord.Text);
            double y = Convert.ToDouble(yCoord.Text);

            if(x > 0)
            {
                if (y > 0)
                {
                    output.InnerHtml = "(" + x + ", " + y + ") falls in quandrant 1.";
                }
                else
                {
                    output.InnerHtml = "(" + x + ", " + y + ") falls in quandrant 2.";
                }
            }
            else
            {
                if (y > 0)
                {
                    output.InnerHtml = "(" + x + ", " + y + ") falls in quandrant 4.";
                }
                else
                {
                    output.InnerHtml = "(" + x + ", " + y + ") falls in quandrant 3.";
                }
            }
        }
    }
}