﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CartesianSmartesian.aspx.cs" Inherits="BonusAssignment.CartesianSmartesian" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label AssociatedControlID="xCoord" runat="server">Input x-coordinate</asp:Label>
            <asp:TextBox ID="xCoord" runat="server" type="number" placeholder="eg: 3"></asp:TextBox>
            <asp:RequiredFieldValidator ControlToValidate="xCoord" ErrorMessage="Please provide an input" runat="server"></asp:RequiredFieldValidator>
            <asp:CompareValidator ControlToValidate="xCoord" ValueToCompare="0" operator="NotEqual" ErrorMessage="Zero(0) is an invalid input" runat="server"></asp:CompareValidator>
        </div>
        <div>
            <asp:Label AssociatedControlID="yCoord" runat="server">Input y-coordinate</asp:Label>
            <asp:TextBox ID="yCoord" runat="server" type="number" placeholder="eg: -6"></asp:TextBox>
            <asp:RequiredFieldValidator ControlToValidate="yCoord" ErrorMessage="Please provide an input" runat="server"></asp:RequiredFieldValidator>
            <asp:CompareValidator ControlToValidate="yCoord" ValueToCompare="0" Operator="NotEqual" ErrorMessage="Zero(0) is an invalid input" runat="server"></asp:CompareValidator>
        </div>
        <div>
            <asp:Button id="btn" text="Find the quandrant" OnClick="Find_Quadrant" runat="server" />
        </div>
        <div id="output" runat="server"></div>
    </form>
</body>
</html>
