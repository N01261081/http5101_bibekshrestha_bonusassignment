﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DivisibleSizzable.aspx.cs" Inherits="BonusAssignment.DivisibleSizzable" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label AssociatedControlID="inputNumber" runat="server">Input a positive number</asp:Label>
            <asp:TextBox ID="inputNumber" runat="server" type="number" placeholder="eg: 355"></asp:TextBox>
            <asp:RequiredFieldValidator ControlToValidate="inputNumber" ErrorMessage="Please provide an input" runat="server"></asp:RequiredFieldValidator>
            <asp:CompareValidator ControlToValidate="inputNumber" ValueToCompare="0" Operator="GreaterThan" ErrorMessage="Zero(0) or a negative number is not a valid input" runat="server"></asp:CompareValidator>
        </div>
        <div>
            <asp:Button type="submit" Text="Find if Prime" OnClick="Find_Prime" runat="server" />
        </div>
        <div id="output" runat="server"></div>
    </form>
</body>
</html>
