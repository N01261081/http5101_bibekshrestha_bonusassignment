﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssignment
{
    public partial class DivisibleSizzable : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void Find_Prime(object sender, EventArgs e)
        {
            //Check to see if the inputs are all valid
            if (!Page.IsValid)
            {
                return;
            }

            //Define variables and assign inputs
            int userNumber = Convert.ToInt32(inputNumber.Text);

            int d = userNumber - 1;
            Boolean prime = true;

            while(d > 1)
            {
                if (userNumber%d == 0)
                {
                    prime = false;
                }

                d--;
            }

            if (prime) { output.InnerHtml = "The number is Prime"; }
            else { output.InnerHtml = "The number is not Prime"; }
        }
    }
}