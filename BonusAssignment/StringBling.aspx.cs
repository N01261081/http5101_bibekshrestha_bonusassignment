﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssignment
{
    public partial class StringBlind : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void Find_Palindrome(object sender, EventArgs e)
        {
            if (!Page.IsValid) { return; }

            string userInput = inputString.Text.ToString().ToLower().Replace(" " , string.Empty);
            string revString = ReverseString(userInput);
            if(userInput == revString) { output.InnerHtml = "The string is a palindrome"; }
            else { output.InnerHtml = "The string is not a palindrome"; }
        }
        public static string ReverseString(string s)
        {
            char[] somearay = s.ToCharArray();
            Array.Reverse(somearay);
            return new string(somearay);
        }
    }
}