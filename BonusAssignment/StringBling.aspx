﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StringBling.aspx.cs" Inherits="BonusAssignment.StringBlind" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label AssociatedControlID="inputString" runat="server">Please input your string</asp:Label>
            <asp:TextBox ID="inputString" runat="server" placeholder="eg: Race car"></asp:TextBox>
            <asp:RequiredFieldValidator ControlToValidate="inputString" runat="server" ErrorMessage="Please provide an input"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator runat="server" ControlToValidate="inputString" ValidationExpression="^[a-zA-Z ]+$" ErrorMessage="That is not a valid string"></asp:RegularExpressionValidator>
        </div>
        <div>
            <asp:Button runat="server" type="submit" Text="Find if Palindrome" onclick="Find_Palindrome"/>
        </div>
        <div id="output" runat="server"></div>
    </form>
</body>
</html>
